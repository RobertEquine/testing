package utili;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class JavaScriptUtil 
{
	
WebDriver driver;

	
public JavaScriptUtil(WebDriver driver) 
{
	
this.driver = driver;
		
}
	
/**
* This method will scroll the page until the element is not visible on the page
* @param WebElement variable
* @param WebDriver variable
*/
public void scrollIntoView(WebElement element, WebDriver driver) 
{
JavascriptExecutor js = ((JavascriptExecutor) driver);
js.executeScript("arguments[0].scrollIntoView(true);", element);

}//scrollIntoView


/**
* This method will flash the specified Web Element
* @param WebElement variable
*/
public void flashWebelement(WebElement element) 
{
JavascriptExecutor js = ((JavascriptExecutor) driver);
String bgcolor = element.getCssValue("backgroundColor");
for (int i = 0; i < 20; i++) 
{
	
changeColorElement("rgb(0,200,0)", element);// 1
changeColorElement(bgcolor, element);// 2

}//for

}//flashWebelement


/**
* This method will change the existing color of web element to the specified color in the argument String colour
* @param WebElements color
* @param WebElement variable
*/
public void changeColorElement(String color, WebElement element) 
{
JavascriptExecutor js = ((JavascriptExecutor) driver);
js.executeScript("arguments[0].style.backgroundColor = '" + color + "'", element);

try 
{
Thread.sleep(20);
} 
catch (InterruptedException e) 
{
		
}//catch

}//changeColorElement


/**
* This method will draw a solid red color border to the Web Element
* @param WebElement variable
*/
public void drawBorderWebElement(WebElement element) 
{
	
JavascriptExecutor js = ((JavascriptExecutor) driver);
js.executeScript("arguments[0].style.border='3px solid red'", element);

}//drawBorderWebElement

	
/**
* This method will generate an alert on current web page with String value as parameter message
* @param message
*/
public void generateAlertWebPage(String message) 
{
	
JavascriptExecutor js = ((JavascriptExecutor) driver);
js.executeScript("alert('" + message + "')");

}//generateAlertWebPage

	
/**
* This method will click on the Web Element specified in the argument
* @param WebElement element
*/
public void clickElementByJS(WebElement element) 
{
	
JavascriptExecutor js = ((JavascriptExecutor) driver);
js.executeScript("arguments[0].click();", element);

}//clickElementByJS

	
/**
* This method will refresh the page
*/
public void refreshBrowserByJS() 
{
	
JavascriptExecutor js = ((JavascriptExecutor) driver);
js.executeScript("history.go(0)");

}//refreshBrowserByJS


/**
* This method will return the current title of web page as String
* @return String
*/
public String getTitleByJS() 
{
	
JavascriptExecutor js = ((JavascriptExecutor) driver);
String currentTitle = js.executeScript("return document.title;").toString();
return currentTitle;

}//getTitleByJS

	
/**
* This method will return the Inner Page text available for the current HTML page
* @return String
*/
public String getPageInnerText() 
{
	
JavascriptExecutor js = ((JavascriptExecutor) driver);
String pageText = js.executeScript("return document.documentElement.innerText;").toString();
return pageText;

}//getPageInnerText



/**
* This method will scroll the page to the bottom 
*/
public void scrollPageDown() 
{
	
JavascriptExecutor js = ((JavascriptExecutor) driver);
js.executeScript("window.scrollTo(0,document.body.scrollHeight)");

}//scrollPageDown


/**
* This method will return the browser information in the form if String
* @return String
*/
public String getBrowserInfo() 
{
	
JavascriptExecutor js = ((JavascriptExecutor) driver);
String uAgent = js.executeScript("return navigator.userAgent;").toString();
return uAgent;

}//getBrowserInfo



/**
* This method will send text to the Web Element which is having an HTML id attribute is available
* @param String id
* @param String value
*/
public void sendKeysUsingJSWithId(String id, String value) 
{
	
JavascriptExecutor js = ((JavascriptExecutor) driver);
js.executeScript("document.getElementById('" + id + "').value='" + value + "'");

}//sendKeysUsingJSWithId

	
/**
* This method will send text to the Web Element which is having an HTML name attribute is available
* @param String name
* @param String value
*/
public void sendKeysUsingJSWithName(String name, String value) 
{
	
JavascriptExecutor js = ((JavascriptExecutor) driver);
js.executeScript("document.getElementByName('" + name + "').value='" + value + "'");

}//sendKeysUsingJSWithName


public void checkPageIsReady() 
{
JavascriptExecutor js = (JavascriptExecutor) driver;
// Initially bellow given if condition will check ready state of page.
if (js.executeScript("return document.readyState").toString().equals("complete"))
{
System.out.println("Page Is loaded.");
return;
}//if

// This loop will rotate for 25 times to check If page Is ready after
// every 1 second.
// You can replace your value with 25 If you wants to Increase or
// decrease wait time.
for (int i = 0; i < 25; i++) 
{
try 
{
	
Thread.sleep(1000);

}//try

catch (InterruptedException e) 
{
	
}//catch
// To check page ready state.
if (js.executeScript("return document.readyState").toString().equals("complete")) 
{
break;
}//if

}//for

}//checkPageIsReady

}//JavaScriptUtil
