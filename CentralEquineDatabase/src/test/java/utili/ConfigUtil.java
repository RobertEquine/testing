package utili;
import java.io.FileInputStream;
import java.util.Properties;
import helper.Constants;

public class ConfigUtil extends Constants
{
	
public Properties LoadProperties()
{
		
try
{
FileInputStream fiStream = new FileInputStream(CONFIG_FILE_PATH);
Properties prop = new Properties();
prop.load(fiStream);
return prop;
}//try
catch(Exception e)
{
		
System.out.println("File not found expection thrown for config.properties file.");
return null;

}//catch
		
}//LoadProperties	
	
	
}//ConfigUtil
