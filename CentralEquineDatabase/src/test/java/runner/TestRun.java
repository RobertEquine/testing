package runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;

@RunWith(Cucumber.class) 
@CucumberOptions(features= "src/test/resources/Features",
glue= "stepdefs",
monochrome = true,
plugin = {"pretty","html:target/HtmlReports","json:target/JSONReports/report.json","junit:target/JUnitReports/report.xml",
"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
tags="@regression",
publish = false,
dryRun=false
)
public class TestRun 
{
	


	

}//TestRun
