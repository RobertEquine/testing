package utils.ui;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 
 * @author robert
 * Library created to interact with the elements
 */

public abstract class Interact 
{

private static final Logger logger = LogManager.getLogger(Interact.class);
private WebDriver driver;

public WebDriver getDriver() 
{
	
return driver;

}//getDriver


	
public void setDriver(WebDriver driver) 
{
	
this.driver=driver;

}//setDriver


public WebElement clickElement(By by) 
{
	
WebDriverWait wait = new WebDriverWait(driver, 60);
WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
element.click();
logger.info("Element is clicked. Element Description: " + by.toString());
return element;

}//clickElement
	
	
public void clickElement(WebElement webElement) 
{
	
WebDriverWait wait = new WebDriverWait(driver, 60);
WebElement element = wait.until(ExpectedConditions.elementToBeClickable(webElement));
webElement.click();
logger.info("Element is clicked. Element Description: " + webElement.toString());

}//clickElement


public WebElement setElement(By by, String text) 
{
	
WebDriverWait wait = new WebDriverWait(driver, 60);
WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
element.sendKeys(text);
logger.info("Element is Set with text as: " + text + ". Element Description: " + by.toString());
return element;

}//setElement

public WebElement setElement(WebElement webElement, String text) 
{
	
WebDriverWait wait = new WebDriverWait(driver, 60);
WebElement element = wait.until(ExpectedConditions.elementToBeClickable(webElement));
element.sendKeys(text);
logger.info("Element is Set with text as: " + text + ". Element Description: " + webElement.toString());
return element;


}//setElement


public String getAttribute(By by, String attName) 
{
	
WebDriverWait wait = new WebDriverWait(driver, 60);
WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
String value = element.getAttribute(attName);
logger.info("Get Attribute for element: " + by.toString() + " Attribute name: " + attName);
return value;

}//getAttribute

public String getAttribute(WebElement webElement, String attName) 
{
	
WebDriverWait wait = new WebDriverWait(driver, 60);
WebElement element = wait.until(ExpectedConditions.elementToBeClickable(webElement));
String value = element.getAttribute(attName);
logger.info("Get Attribute for element: " + webElement.toString() + " Attribute name: " + attName);
return value;

}//getAttribute


public String getText(By by) 
{
	
WebDriverWait wait = new WebDriverWait(driver, 60);
WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
String value = element.getText();
logger.info("Get Text for element: " + by.toString() + " Text : " + value);
return value;

}//getText

public String getText(WebElement webElement) 
{
	
WebDriverWait wait = new WebDriverWait(driver, 60);
WebElement element = wait.until(ExpectedConditions.elementToBeClickable(webElement));
String value = element.getText();
logger.info("Get Text for element: " + webElement.toString() + " Text : " + value);
return value;

}//getText

public int getElementsCount(By by) 
{

List<WebElement> links = driver.findElements(by);
long count = links.stream().filter(item -> item.isDisplayed()).count();
Integer value = (int) (long) count;
logger.info("Get count for element: " +  by.toString() + " Count : " + value);
return value;

}//getElementsCount

public int getElementsCountByName(By by,String text) 
{

List<WebElement> Elements = driver.findElements(by);
List<WebElement> filteredList = Elements.stream().filter(element -> element.getText().contains(text)).
collect(Collectors.toList());
int value = filteredList.size();
logger.info("Get count for element: " +  by.toString() + " Count : " + value);
return value;

}//getElementsCountByName

public void ClickElementFromList(By by,String text) 
{

List<WebElement> links = driver.findElements(by);
links.stream().filter(element -> element.getText().contains(text))
.forEach(element ->element.click());
logger.info("Clicked on the element: " +  by.toString());

}//ClickElementFromList


public List<WebElement> getListOfWebElements(By by)
{
	
WebDriverWait wait = new WebDriverWait(driver, 60);
WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
logger.info("List of Elements returned for description: " + by.toString());
return element.findElements(by);

}//getListOfWebElements


public boolean validateElementIsDisplayed(By by) 
{
	
boolean flag = driver.findElement(by).isDisplayed();
logger.info("Element is Displayed status: " + by.toString());
return flag;

}//validateElementIsDisplayed


public void switchToWindowTab(int id) 
{
	
String [] handles = (String[]) driver.getWindowHandles().toArray();
driver.switchTo().window(handles[id]);
logger.info("Browser Switched to second window tab.");

}//switchToSecondWindowTab
	
	
public void switchToDefaultWindowTab() 
{
	
String [] handles = (String[]) driver.getWindowHandles().toArray();
driver.switchTo().window(handles[0]);
logger.info("Browser Switched to parent window tab.");

}//switchToDefaultWindowTab

public String switchToWindowBasedOnTitle(WebDriver driver, String title)
{
	
return	driver.getWindowHandles()
.stream()
.map(handler -> driver.switchTo().window(handler).getTitle())
.filter(ele -> ele.contains(title))
.findFirst()
.orElseThrow(() -> {
throw new RuntimeException("No Such window");
});

}//switchToWindowBasedOnTitle

	
public byte[] takeScreenShot() 
{
TakesScreenshot shot = (TakesScreenshot)driver;
logger.info("Screen Shot taken for full driver. ");
return shot.getScreenshotAs(OutputType.BYTES);

}//takeScreenShot

	
public byte[] takeScreenShot(By by) 
{
	
TakesScreenshot shot = (TakesScreenshot)driver.findElement(by);
logger.info("Screen Shot taken for element: " + by.toString() );
return shot.getScreenshotAs(OutputType.BYTES);

}//takeScreenShot

	
public File takeScreenShotAsFile() 
{
	
TakesScreenshot shot = (TakesScreenshot)driver;
logger.info("Screen Shot taken for full driver and returned as a file.");
return shot.getScreenshotAs(OutputType.FILE);

}//takeScreenShotAsFile
	
	
public File takeScreenShotAsFile(By by) 
{
	
TakesScreenshot shot = (TakesScreenshot)driver.findElement(by);
logger.info("Screen Shot taken for element and returned as a file. By descp: " + by.toString());
return shot.getScreenshotAs(OutputType.FILE);

}//takeScreenShotAsFile


//To get random Key
public String GetRandomString(int n) 
{
//lower limit for LowerCase Letters 
int lowerLimit = 97; 

//lower limit for LowerCase Letters 
int upperLimit = 122; 

Random random = new Random(); 

//Create a StringBuffer to store the result 
StringBuffer r = new StringBuffer(n); 

for (int i = 0; i < n; i++) { 

//take a random value between 97 and 122 
int nextRandomChar = lowerLimit + (int)(random.nextFloat() * (upperLimit - lowerLimit + 1)); 

//append a character at the end of bs 
r.append((char)nextRandomChar); 
} 

//return the resultant string 
return r.toString(); 

}//GetRandomString


public String randomString()
{

String generatedstring = RandomStringUtils.randomAlphabetic(8);
return generatedstring;
	
}//randomString

public String randomNumber()
{
	
String generatednumber = RandomStringUtils.randomNumeric(4);
return generatednumber;

}//randomNumber


public boolean validateSortedWebTable(By clickSortButton,List<WebElement> WebTable)
{

//Click for sorting the table
clickElement(clickSortButton);	

//Capture text of all WebElemnts into new(Orginal) list
List<String> orginalTextList = WebTable.stream().map(s -> s.getText()).collect(Collectors.toList());	

//Sort on the original list of step 3 -> Sorted list
List<String> sortedList =  orginalTextList.stream().sorted().collect(Collectors.toList());	

//Compare original list vs Sorted list
boolean flag = orginalTextList.equals(sortedList);
logger.info("WebTable is sorted correctly: " + flag);
return flag;

}//validateSortedWebTable

public void paginationAndFindElementText(List<WebElement> ElementList,By nextButton,String text,By childObject)
{

List<String>  item;
do
{
	
//Scan the name column with get text 
item = ElementList.stream().filter(s -> s.getText().contains(text)).
map(s -> getTextFromPage(s,childObject)).collect(Collectors.toList());


//Pagination in Web Table scan each page to find the object
if(item.size()<1)
{
//If the item is not in the page click in next
clickElement(nextButton);	

		
}//item

}//do

while(item.size()<1);

logger.info("Pagination and find the obejct in the page");


}//pagination

private static String getTextFromPage(WebElement s,By by)
{
	
String pricevalue = s.findElement(by).getText();
return pricevalue;


}//getTextFromPage
	
	
}//Interact
