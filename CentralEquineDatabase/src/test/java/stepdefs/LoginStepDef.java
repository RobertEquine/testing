package stepdefs;

import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter;
import context.Base;
import context.TestContextUI;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utilis.manager.driver.factory.DriverFactory;
import utilis.manager.driver.factory.DriverManager;

public class LoginStepDef extends Base
{
	
TestContextUI testContextUI;
DriverManager driverManager;
Logger logger = LogManager.getLogger(this.getClass()); 

public LoginStepDef(TestContextUI testContextUI)
{
	
this.testContextUI = testContextUI;

}//LoginStepDef
	
	
@Given("^browser is open$")
public void browser_is_open() throws Throwable 
{
driverManager = DriverFactory.getDriverManager("chrome");
WebDriver driver = driverManager.getDriver();
driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
driverManager.maximizeBrowser();
ExtentCucumberAdapter.addTestStepLog("Browser is opened");
testContextUI.setDriver(driver);
testContextUI.intitializePageObject(driver);

}//browser_is_open

@And("^user is on login page$")
public void user_is_on_login_page() throws Throwable 
{

driverManager.navigateToDriver(url);
logger.info("Opening URL: " +driverManager.getCurrentUrl());	
ExtentCucumberAdapter.addTestStepLog("Opening URL: " +driverManager.getCurrentUrl());	
	
}//user_is_on_login_page

@When("^user enter (.+) and (.+)$")
public void user_enter_and(String username, String password) throws Throwable 
{
  
testContextUI.getLoginPage().SetEmail(username);	
testContextUI.getLoginPage().SetPassword(password);
logger.info("User entered the username and password");
ExtentCucumberAdapter.addTestStepLog("User entered the username and password");	
	
}//user_enter_and

@And("^user clicks on login$")
public void user_clicks_on_login() throws Throwable 
{
  
testContextUI.getLoginPage().ClickOnLoginButton();
logger.info("User clicked on login button");
ExtentCucumberAdapter.addTestStepLog("User clicked on login button");
	
}//user_clicks_on_login

@Then("^user navigate to the App Home page$")
public void user_navigate_to_the_app_home_page() throws Throwable 
{
	
logger.info("User navigated to home page");
ExtentCucumberAdapter.addTestStepLog("User navigated to home page");
testContextUI.getLoginPage().validateHomePageDisplayed();

}//user_navigate_to_the_app_home_page
	
	
	
}//LoginStepDef
