package stepdefs;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter;
import context.Base;
import context.TestContextUI;
import helper.GenericFunctions;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class Hooks extends Base
{
	
TestContextUI testContextUI;
Scenario scenario;
	
	
public Hooks(TestContextUI testContextUI)
{
						
this.testContextUI = testContextUI;

}//Hooks	

	
@Before()
public void setUp(Scenario scenario) throws InterruptedException, IOException
{	

this.scenario = scenario;
if ((new File(screenshotdir)).exists())
FileUtils.cleanDirectory(new File(screenshotdir));

}//setUp

	
@After
public void tearDown(Scenario scenario) throws InterruptedException, IOException, IllegalMonitorStateException
{
	
if(scenario.isFailed())	
{
	
ExtentCucumberAdapter.addTestStepScreenCaptureFromPath(GenericFunctions.getBase64Screenshot(testContextUI.getDriver()));
ExtentCucumberAdapter.addTestStepLog("Screenshot attached");

}//scenario.isFailed

testContextUI.getDriver().quit();	
ExtentCucumberAdapter.addTestStepLog("Browser is closed");

}//tearDown
	
}//Hooks
