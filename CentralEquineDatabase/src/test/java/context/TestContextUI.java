package context;


import org.openqa.selenium.WebDriver;
import pageobjects.LoginPageObjects;


public class TestContextUI 
{

private WebDriver driver;
private LoginPageObjects login;



public WebDriver getDriver() 
{
	
return driver;

}//getDriver

public void setDriver(WebDriver driver) 
{
	
this.driver = driver;

}//setDriver

public LoginPageObjects getLoginPage() 
{
	
return login;
	
}//getLoginPage

public void intitializePageObject(WebDriver driver)
{
	
login = new LoginPageObjects(driver);


	
}//intitializePageObject

	
}//TestContextUI
