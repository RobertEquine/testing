package pageobjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.ui.Interact;

public class LoginPageObjects extends Interact 
{
	
private static final Logger logger = LogManager.getLogger(Interact.class);
	
public LoginPageObjects(WebDriver driver) 
{
	
setDriver(driver);

}//LoginPageObjects	

private By userNameText = By.id("username");
private By passwordText = By.id("password");	
private By loginButton = By.xpath("//button[@class='btn save-button login-button']");	
private By feedback = By.xpath("//a[contains(text(),'feedback')]");


public void SetEmail(String userName)
{

setElement(userNameText,userName);
logger.info("User Entered the User Name: " + userName);

}//SetEmail

public void SetPassword(String password)
{

setElement(passwordText,password);
logger.info("User Entered the User Name: " + password);
	
}//SetPassword

public void ClickOnLoginButton() 
{

clickElement(loginButton);
logger.info("Clicked on Login Button");

}//ClickOnLoginButton

public void validateHomePageDisplayed() 
{

validateElementIsDisplayed(feedback);
logger.info("Home page is displayed");

}//validateHomePageDisplayed

	
	
	
}//LoginPageObjects
