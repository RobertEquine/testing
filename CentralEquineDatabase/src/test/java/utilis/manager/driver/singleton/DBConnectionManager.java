package utilis.manager.driver.singleton;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionManager 
{
	
private static DBConnectionManager instance;
private Connection conn;
	
	
private DBConnectionManager(String url) 
{

try 
{
	
Class.forName("com.mysql.cj.jdbc.Driver");
this.conn = DriverManager.getConnection(url);

}//try

catch (Exception e) 
{
	
// TODO Auto-generated catch block
e.printStackTrace();

}//e

}//catch
	
	
public Connection getConnection() 
{
	
return this.conn;
	
}//getConnection


	
public static DBConnectionManager getInstance(String url) throws SQLException 
{
if (instance==null) 
{
	
instance= new DBConnectionManager(url);

}//if

else if (instance.getConnection().isClosed()) 
{
	
instance= new DBConnectionManager(url);

}//else if

return instance;

}//getInstance

}//DBConnectionManager
