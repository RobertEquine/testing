package utilis.manager.driver.factory;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FirefoxManager extends DriverManager
{

@Override
protected void initDriver() 
{
	
WebDriverManager.firefoxdriver().setup();
driver = new FirefoxDriver();

}
}//FirefoxManager
