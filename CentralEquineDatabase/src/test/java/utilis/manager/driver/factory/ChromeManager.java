package utilis.manager.driver.factory;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ChromeManager extends DriverManager
{

@Override
protected void initDriver() 
{
	
WebDriverManager.chromedriver().setup();
driver = new ChromeDriver();

}

}//ChromeManager
