@LoginFeature
Feature: feature to test login functionality

Background: backround steps
Given browser is open
And user is on login page

@regression
Scenario Outline: Check login is successful with valid credentials
When user enter <username> and <password>
And user clicks on login
Then user navigate to the App Home page
Examples:
|username                       |password     |
|piomanager@equineregister.co.uk|Sc@n!m@L2020 |
